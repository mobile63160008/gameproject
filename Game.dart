import 'dart:io';
import 'Board.dart';
import 'Player.dart';
import 'dart:math';
import 'Board.dart';

class Game {
  Player p1 = new Player("x");
  Board b1 = new Board();
  String? dice1;
  String? dice2;
  String? dice3;
  List<String?> rs = [];
  List<String> table = ["Crab", "Fish", "Grourd", "Tiger", "Chicken", "Shrimp"];
  bool exit = true;
  int wrong = 0;
  int bet = 301;
  int checkbet = 0;

  var gameIcon = (r'''

   ____      ____     _____                                                                                       
  / ___|    / ___|   |  ___|                                                                                      
 | |  _    | |       | |_                                                                                         
 | |_| |   | |___    |  _|                                                                                        
  \____|    \____|   |_|                    _                                 _              __   _         _     
   __ _   _ __    ___    _   _   _ __    __| |           ___   _ __    __ _  | |__          / _| (_)  ___  | |__  
  / _` | | '__|  / _ \  | | | | | '__|  / _` |          / __| | '__|  / _` | | '_ \        | |_  | | / __| | '_ \ 
 | (_| | | |    | (_) | | |_| | | |    | (_| |    _    | (__  | |    | (_| | | |_) |    _  |  _| | | \__ \ | | | |
  \__, | |_|     \___/   \__,_| |_|     \__,_|   ( )    \___| |_|     \__,_| |_.__/    ( ) |_|   |_| |___/ |_| |_|
  |___/                                          |/                                    |/                         

''');
  var crabfishgrourd = (r'''
                __       __              
              / <`     '> \                                                 _,;_;/-",_                                                ___
              (  / @   @ \  )                                              ,")  (  ((O) " /;                                          \ /        
               \(_ _\_/_ _)/                                             ,")  (  ((O) "  . ,                                          / \         
             (\ `-/     \-' /)                                         ,` (    )  ;  -.,/;`}                                        (     )               
              "===\     /==="                                         ,"  O    (  ( (  . -_-.                                        /   \         
               .==')___(`==.                                          `.  ;      ;  ) ) \`; \;                                    (         )   
              ' .='     `=.                                              `., )   (  ( _-`   \,'                                  (           ) 
                                                                            "`'-,,`.jb                                            \_________/
''');
  var tigerchicksh = (r'''
                      __,,,,_                                     __
       _ __..-;''`--/'/ /.',-`-.                                _/o \                                                        _.--------,..
   (`/' ` |  \ \ \\ / / / / .-'/`,_                            /_    |                                                    ;:____________ `--,_
  /'`\ \   |  \ | \| // // / -.,/_,'-,                          W\  /              |////                                (_.-(o)-,-,-.. `''--,_
 /<7' ;  \ \  | ; ||/ /| | \/    |`-/,/-.,_,/')                   \ \  __________||//|/                                   ''-.__)_)_,'`\
/  _.-, `,-\,__|  _-| / \ \/|_/  |    '-/.;.\'                     \ \/         /|/-//-                                       /||//| )-;
`-`  f/ ;      / __/ \__ `/ |__/ |                                  |     -----  // --                                        ||\ |/
     `-'      |  -| =|\_  \  |-' |                                  |      -----   /-                                         ||\ |/                                         
           __/   /_..-' `  ),'  //                                  \            /                                            '' ' (/)
       fL ((__.-'((___..-'' \__.'                                     \_/  \___/                                               
                                                                      ||     || 

''');

  void printGameIcon() {
    int round = 6;
    for (int i = 0; i < round; i++) {
      print('\x1B[31m$gameIcon\x1B[0m');
      delay(100);
      clearScreen();
      print('\x1B[32m$gameIcon\x1B[0m');
      delay(100);
      clearScreen();
      print('\x1B[36m$gameIcon\x1B[0m');
      delay(100);
      clearScreen();
      print('\x1B[33m$gameIcon\x1B[0m');
      delay(100);
      clearScreen();
    }
    print('\x1B[35m$gameIcon\x1B[0m');
  }

  void printTop() {
    print('\x1B[34m$crabfishgrourd\x1B[0m');
  }

  void printBottom() {
    print('\x1B[31m$tigerchicksh\x1B[0m');
  }

  void delay(int time) {
    //Sleep func
    sleep(Duration(milliseconds: time));
  }

  void welcomeTag() {
    clearScreen();
  }

  void clearScreen() {
    print('\x1B[2J\x1B[0;0H');
  }

  void dice() {
    dice1 = b1.randomDice();
    dice2 = b1.randomDice();
    dice3 = b1.randomDice();
  }

  void disMoney(int bet) {
    int now = p1.getMoney();
    now = now - bet;
    p1.setMoney(now);
  }

  void addMoney(int bet) {
    int now = p1.getMoney();
    now = now + (bet * 2);
    p1.setMoney(now);
  }

  void saveDice(String? dice1, String? dice2, String? dice3) {
    rs.add(dice1);
    rs.add(dice2);
    rs.add(dice3);
  }

  void resetDice() {
    rs = [];
  }

  void checkResult(int bet, int press) {
    for (int i = 0; i < 3; i++) {
      if (rs[i] == table[press]) {
        addMoney(bet);
      }
    }
  }

  void runGame() {
    printGameIcon();
    print("Welcome To GCF Game");
    print("Enter your name : ");
    String name = stdin.readLineSync()!;
    clearScreen();
    while (exit == true) {
      printTop();
      printBottom();
      print(
          " [1] Crab [2] Fish [3] Grourd \n [4] Tiger  [5] Chicken  [6] Shrimp");
      print("---------------------------------------------");
      print(
          "$name Select what you want to Bet! : \n Press[1] : Crab \n Press[2] : Fish \n Press[3] : Grourd \n Press[4] : Tiger \n Press[5] : Chicken \n Press[6] : Shrimp ");
      int press = int.parse(stdin.readLineSync()!);
      press = press - 1;
      int money = p1.getMoney();
      print("Start game You have money : $money baht");
      print("Please place a bet : ");
      bet = int.parse(stdin.readLineSync()!);
      while (bet > money) {
        print("ํYou don't have enough money!!! ");
        print("Please place a bet : ");
        bet = int.parse(stdin.readLineSync()!);
      }
      disMoney(bet);
      money = p1.getMoney();
      print("balance : $money");
      print("---------------------");
      delay(1000);
      clearScreen();
      int choice = press + 1;
      print("Your Select is : $choice");
      //Randomลูกเต๋า
      print("RandomDice Please wait!!!");
      dice();
      print("Dice1 : $dice1");
      delay(2000);
      print("Dice2 : $dice2");
      delay(2000);
      print("Dice3 : $dice3");
      saveDice(dice1, dice2, dice3);
      print(rs);
      checkResult(bet, press);
      money = p1.getMoney();
      print("balance : $money baht");
      delay(2000);
      clearScreen();
      resetDice();

      while (wrong == 0) {
        print("ํYou want to Exit this Game? \n Press[Y] Yes Press[N] No");
        String end = stdin.readLineSync()!;
        if (end == "Y" || end == "y") {
          exit = false;
          wrong = 1;
        } else if (end == "N" || end == 'n') {
          exit = true;
          wrong = 1;
        } else {
          print("Unknown this letter!!!");
          wrong = 0;
        }
      }
    }
  }
}
