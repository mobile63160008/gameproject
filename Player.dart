import 'dart:io';

class Player {
  String? name = null;
  int money = 300;
  int amount = 0;

  Player(String name) {
    this.name = name;
  }

  String getName(String name) {
    return name;
  }

  int getMoney() {
    return money;
  }

  void setMoney(int x) {
    money = x;
  }
}
